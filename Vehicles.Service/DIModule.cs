﻿using Vehicles.Service.Models;
using Vehicles.Service.Models.Common;
using Vehicles.Service.Repository;
using Vehicles.Service.Repository.Common;
using Vehicles.Service.Service;
using Vehicles.Service.Service.Common;
using Vehicles.Service.Service;
using Ninject.Modules;
using System;

namespace Vehicles.Service
{
    public class DIModule : NinjectModule
    {
        #region Methods

        public override void Load()
        {
            Bind<IVehicleMakeService>().To<VehicleMakeService>();
            Bind<IVehicleMakeRepository>().To<VehicleMakeRepository>();
            Bind<IVehicleMake>().To<VehicleMake>();
            Bind<IVehicleModelService>().To<VehicleModelService>();
            Bind<IVehicleModelRepository>().To<VehicleModelRepository>();
            Bind<IVehicleModel>().To<VehicleModel>();
        }

        #endregion Methods
    }
}