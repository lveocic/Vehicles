﻿using Vehicles.Service.DAL;
using Vehicles.Service.Models;
using Vehicles.Service.Repository.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles.Service.Repository.Common
{
    public interface IVehicleMakeRepository
    {
        #region Methods
        Task<IEnumerable<VehicleMake>> GetAllAsync();
        bool Delete(Guid id);
        Task<PagedList<VehicleMake>> FindVehicleMake(IVehicleMakeFilter filter);

        Task<VehicleMake> FindAsync(Guid id);

        Task<VehicleMake> InsertAsync(VehicleMake vehicleMake);

        Task UpdateAsync(VehicleMake vehicleMake);

        #endregion Methods
    }
}