﻿using Vehicles.Service.DAL;
using Vehicles.Service.Models;
using Vehicles.Service.Repository.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles.Service.Repository.Common
{
    public interface IVehicleModelRepository
    {
        #region Methods
       
        Task<PagedList<VehicleModel>> FindVehicleModel(IVehicleModelFilter filter);
        Task Delete(Guid id);

        Task<VehicleModel> FindAsync(Guid id);

        Task<VehicleModel> InsertAsync(VehicleModel vehicleModel);

        Task UpdateAsync(VehicleModel vehicleModel);

        #endregion Methods
    }
}