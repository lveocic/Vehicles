﻿using AutoMapper;
using Vehicles.Service.DAL;
using Vehicles.Service.Models;
using Vehicles.Service.Models.Common;
using Vehicles.Service.Repository.Common;
using Vehicles.Service.Repository.Filters;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles.Service.Repository
{
    public class VehicleMakeRepository : IVehicleMakeRepository
    {
        #region Fields

        private VehiclesContext Context;

        #endregion Fields

        #region Constructors

        public VehicleMakeRepository(VehiclesContext context)
        {
            Context = context;
        }

        #endregion Constructors

        #region Properties


        #endregion Properties

        #region Methods

        public bool Delete(Guid id)
        {
            
                var vehicleModel = Context.VehicleMakers.Find(id);
                Context.VehicleMakers.Remove(vehicleModel);
                return Context.SaveChanges() > 0;
            
        }

        public async Task<VehicleMake> FindAsync(Guid id)
        {
            try
            {
                return Mapper.Map<VehicleMake>(await Context.VehicleMakers.FindAsync(id));
            }
            catch(Exception exception)
            {
                throw new Exception($"{this.ToString()} - find failed", exception);
            }
        }

        public async Task<IEnumerable<VehicleMake>> GetAllAsync()
        {
            return Mapper.Map<IEnumerable<VehicleMake>>(await Context.VehicleMakers.ToListAsync());
        }


        public async Task<VehicleMake> InsertAsync(VehicleMake vehicleMake)
        {
            try
            {
                var entity = Mapper.Map<VehicleMakeEntity>(vehicleMake);
                var insert = Context.VehicleMakers.Add(entity);
                await Context.SaveChangesAsync();
                return Mapper.Map<VehicleMake>(insert);
            }
            catch(Exception exception)
            {
                throw new Exception($"{this.ToString()} - insert failed", exception);
            }
        }

        public async Task UpdateAsync(VehicleMake vehicleMake)
        {
            try
            {
                var entity = Mapper.Map<VehicleMakeEntity>(vehicleMake);
                Context.Entry(entity).State = EntityState.Modified;
                await Context.SaveChangesAsync();
            }
            catch(Exception exception)
            {
                throw new Exception($"{this.ToString()} - update failed", exception);
            }
        }
        protected Task<IQueryable<VehicleMakeEntity>> ApplyFilteringAsync(IQueryable<VehicleMakeEntity> query, IVehicleMakeFilter filter)
        {
            if (filter != null)
            {
                if (!string.IsNullOrWhiteSpace(filter.SearchQuery))
                {
                    query = query.Where(x => x.Name.ToLower().Contains(filter.SearchQuery.ToLower()));
                }
                if (filter.Ids != null && filter.Ids.Any())
                {
                    query = query.Where(x => filter.Ids.Contains(x.Id));
                }
            }
            return Task.FromResult(query);
        }

        protected Task<IQueryable<VehicleMakeEntity>> ApplyPagingAsync(IQueryable<VehicleMakeEntity> query, IVehicleMakeFilter filter)
        {
            if (filter != null)
            {
                if (filter.Page.HasValue && filter.PageSize.HasValue)
                {
                    query = query.Skip((filter.Page.Value - 1) * filter.PageSize.Value).Take(filter.PageSize.Value);
                }
            }
            return Task.FromResult(query);
        }

        protected Task<IQueryable<VehicleMakeEntity>> ApplySortingAsync(IQueryable<VehicleMakeEntity> query, IVehicleMakeFilter filter)
        {
            if (filter?.OrderBy == nameof(IVehicleMake.Name))
            {
                query = filter?.OrderDirection == "asc" ? query.OrderBy(x => x.Name) : query.OrderByDescending(x => x.Name);
            }
            else
            {
                query = query.OrderBy(x => x.Name);
            }
            return Task.FromResult(query);
        }

        public async Task<PagedList<VehicleMake>> FindVehicleMake(IVehicleMakeFilter filter)
        {
            try
            {
                IQueryable<VehicleMakeEntity> query = Context.VehicleMakers;
                query = await ApplyFilteringAsync(query, filter);
                query = await ApplySortingAsync(query, filter);
                int count = await query.CountAsync();
                query = await ApplyPagingAsync(query, filter);
                var result = await query.ToListAsync();
                var mapped = Mapper.Map<List<VehicleMake>>(result);
                PagedList<VehicleMake> pagedList = new PagedList<VehicleMake>(mapped, filter.Page.Value, filter.PageSize.Value, count);
                return pagedList;
            }
            catch (Exception exception)
            {
                throw new Exception($"{this.ToString()} - find failed", exception);
            }
        }

        #endregion Methods
    }
}