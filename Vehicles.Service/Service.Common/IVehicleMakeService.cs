﻿using Vehicles.Service.Models;
using Vehicles.Service.Repository.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles.Service.Service.Common
{
    public interface IVehicleMakeService
    {
        #region Methods
        Task<IEnumerable<VehicleMake>> GetAllVehicleMakersAsync();
        Task<PagedList<VehicleMake>> SearchVehicleMakers(IVehicleMakeFilter filter);
        bool DeleteVehicleMake(Guid id);

        Task<VehicleMake> FindVehicleMakeAsync(Guid id);

        Task<VehicleMake> InsertVehicleMakeAsync(VehicleMake vehicleMake);

        Task UpdateVehicleMakeAsync(VehicleMake vehicleMake);

        #endregion Methods
    }
}