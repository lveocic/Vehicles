﻿using AutoMapper;
using Vehicles.Service.DAL;
using Vehicles.Service.Models;
using Vehicles.Service.Repository.Common;
using Vehicles.Service.Repository.Filters;
using Vehicles.Service.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles.Service.Service
{
    public class VehicleMakeService : IVehicleMakeService
    {
        #region Constructors

        public VehicleMakeService(IVehicleMakeRepository vehicleMakeRepository)
        {
            VehicleMakeRepository = vehicleMakeRepository;
        }

        #endregion Constructors

        #region Properties

        public IVehicleMakeRepository VehicleMakeRepository { get; set; }

        #endregion Properties

        #region Methods

        public bool DeleteVehicleMake(Guid id)
        {
           return VehicleMakeRepository.Delete(id);
        }
        public async Task<VehicleMake> FindVehicleMakeAsync(Guid id)
        {
            return await VehicleMakeRepository.FindAsync(id);
        }

        public async Task<VehicleMake> InsertVehicleMakeAsync(VehicleMake vehicleMake)
        {
            CreateVehicleMake(vehicleMake);
            var result = await VehicleMakeRepository.InsertAsync(vehicleMake);
            return result;
        }

        public async Task UpdateVehicleMakeAsync(VehicleMake vehicleMake)
        {   
            await VehicleMakeRepository.UpdateAsync(vehicleMake);
        }

        private void CreateVehicleMake(VehicleMake vehicleMake)
        {
            vehicleMake.Id = Guid.NewGuid();
            vehicleMake.Abrv = vehicleMake.Name.ToLower().Replace(" ", "-").Replace("č", "c").Replace("ć", "c").Replace("ž", "z").Replace("š", "s").Replace("đ", "d");
        }

        public async Task<PagedList<VehicleMake>> SearchVehicleMakers(IVehicleMakeFilter filter)
        {
            return await VehicleMakeRepository.FindVehicleMake(filter);
        }

        public async Task<IEnumerable<VehicleMake>> GetAllVehicleMakersAsync()
        {
            return await VehicleMakeRepository.GetAllAsync();
        }

        #endregion Methods
    }
}
