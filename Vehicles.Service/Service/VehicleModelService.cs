﻿using AutoMapper;
using Vehicles.Service.DAL;
using Vehicles.Service.Models;
using Vehicles.Service.Repository.Common;
using Vehicles.Service.Repository.Filters;
using Vehicles.Service.Service.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles.Service.Service
{
    public class VehicleModelService : IVehicleModelService
    {
        #region Constructors

        public VehicleModelService(IVehicleModelRepository vehicleModelRepository)
        {
            VehicleModelRepository = vehicleModelRepository;
        }

        #endregion Constructors

        #region Properties

        public IVehicleModelRepository VehicleModelRepository { get; set; }

        #endregion Properties

        #region Methods

        public async Task DeleteVehicleModel(Guid id)
        {
            await VehicleModelRepository.Delete(id);
        }

        public async Task<VehicleModel> FindVehicleModelAsync(Guid id)
        {
            return await VehicleModelRepository.FindAsync(id);    
        }

        public async Task<VehicleModel> InsertVehicleModelAsync(VehicleModel vehicleModel)
        {
            CreateVehicleModel(vehicleModel);
            var result = await VehicleModelRepository.InsertAsync(vehicleModel);
            return result;
        }

        public async Task UpdateVehicleModelAsync(VehicleModel vehicleModel)
        { 
            await VehicleModelRepository.UpdateAsync(vehicleModel);
        }

        private void CreateVehicleModel(VehicleModel vehicleModel)
        {
            vehicleModel.Id = Guid.NewGuid();
            vehicleModel.Abrv = vehicleModel.Name.ToLower().Replace(" ", "-").Replace("č", "c").Replace("ć", "c").Replace("ž", "z").Replace("š", "s").Replace("đ", "d");
        }

        public async Task<PagedList<VehicleModel>> SearchVehicleModels (IVehicleModelFilter filter)
        {
            return await VehicleModelRepository.FindVehicleModel(filter);
        }
        #endregion Methods
    }
}