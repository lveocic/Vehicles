﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Vehicles.MVC.Mapping;

namespace Vehicles.MVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        #region Methods

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.RegisterMappings();
        }

        #endregion Methods
    }
}