﻿using AutoMapper;
using Vehicles.MVC.Models;
using Vehicles.Service.DAL;
using Vehicles.Service.Models;
using Vehicles.Service.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vehicles.MVC.Mapping
{
    public class AutoMapperConfig
    {

        public static void RegisterMappings()
        {
            AutoMapper.Mapper.Initialize(config =>
            {
                config.CreateMap<IVehicleModel, VehicleModelRestModel>().ReverseMap();
                config.CreateMap<VehicleModel, VehicleModelRestModel>().ReverseMap();
                config.CreateMap<IVehicleMake, VehicleMakeRestModel>().ReverseMap();
                config.CreateMap<VehicleMake, VehicleMakeRestModel>().ReverseMap();
                config.CreateMap<VehicleMake, VehicleMakeEntity>().ReverseMap();
                config.CreateMap<IVehicleMake, VehicleMakeEntity>().ReverseMap();
                config.CreateMap<VehicleModel, VehicleModelEntity>().ReverseMap();
                config.CreateMap<IVehicleModel, VehicleModelEntity>().ReverseMap();
            });

        }
    }
}